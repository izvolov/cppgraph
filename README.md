[:gb: English version](README.en.md)

Графовая библиотека для C++
===========================

[Попробовать онлайн на Wandbox.org](https://wandbox.org/permlink/Q7Mix7V04nPz1o45)

Содержание
==========

1. [Быстрый старт](#быстрый-старт)
2. [Установка](#установка)
    1. [CMake FetchContent](#вариант-1-cmake-fetchcontent)
    2. [Установить с помощью CMake](#вариант-2-установить-с-помощью-cmake)
    3. [Скопировать исходники](#вариант-3-скопировать-исходники)
    3. [Подключить папку с проектом в CMake](#вариант-4-подключить-папку-с-проектом-в-cmake)
3. [Требования](#требования)

Быстрый старт
=============

Пример
------

```cpp
#include <cppgraph/depth_first_search.hpp>
#include <cppgraph/dfs_visitor.hpp>
#include <cppgraph/directed_graph.hpp>

#include <cassert>
#include <set>

// Определим своего посетителя вершин:
template <typename Vertex>
struct vertex_discoverer: cppgraph::dfs_visitor_t
{
    template <typename Graph>
    void discover_vertex (const Graph &, const Vertex & v)
    {
        vertices.insert(v); // При посещении вершины будем
                            // запоминать эту вершину.
    }

    std::multiset<Vertex> vertices;
};

int main ()
{
    const auto graph =
        cppgraph::make_directed_graph<int> // Создадим граф со следующими рёбрами:
        ({
            {0, 1}, {0, 2}, {0, 4},        // 0 -> 1, 0 -> 2, 0 -> 4,
            {1, 3},                        // 1 -> 3,
            {2, 3}, {2, 9}                 // 2 -> 3, 2 -> 9
        });
                                           // В графе также окажутся вершины,
                                           // соответствующие эти рёбрам:
                                           // 0, 1, 2, 3, 4, 9

    // Выполним поиск в глубину с определённым выше посетителем.
    const auto discovered =
        cppgraph::depth_first_search(graph, vertex_discoverer<int>{});

    // Отдельно возьмём все вершины графа в явном виде.
    const auto expected =
        std::multiset<int>
        {
            vertices(graph).begin(),
            vertices(graph).end()
        };
    // Проверим, что при поиске в глубину были посещены все вершины.
    assert(discovered.vertices == expected);
}
```

Установка
=========

Возможны следующие четыре варианта установки.

Вариант 1: CMake FetchContent
-----------------------------

Начиная с версии CMake 3.14 можно скачать и подключить репозиторий с зависимостью прямо во время сборки с помощью модуля [FetchContent](https://cmake.org/cmake/help/v3.14/module/FetchContent.html). В случае с библиотекой CppGraph это можно записать тремя командами:

```cmake
include(FetchContent)
FetchContent_Declare(CppGraph GIT_REPOSITORY https://gitlab.com/izvolov/cppgraph.git)
FetchContent_MakeAvailable(CppGraph)
```

Этот набор команд породит интерфейсную библиотеку `CppGraph::cppgraph`, которую можно использовать при подключении библиотек:

```cmake
add_executable(program program.cpp)
target_link_libraries(program PRIVATE CppGraph::cppgraph)
```

Вариант 2: Установить с помощью CMake
-------------------------------------

```shell
cd path/to/build/directory
cmake -DCMAKE_BUILD_TYPE=Release path/to/cppgraph
make
make install
```

После этого в системе сборки CMake будет доступен пакет `CppGraph`:

```cmake
find_package(CppGraph)
```

Эта команда породит интерфейсную библиотеку `CppGraph::cppgraph`, которую можно использовать при подключении библиотек:

```cmake
add_executable(program program.cpp)
target_link_libraries(program PRIVATE CppGraph::cppgraph)
```

Вариант 3: Скопировать исходники
--------------------------------

Поскольку CppGraph — полностью заголовочная библиотека, то достаточно скопировать в нужную директорию все заголовки из папки `include` из [репозитория](https://gitlab.com/izvolov/cppgraph) и подключить их в свой проект.

Вариант 4: Подключить папку с проектом в CMake
----------------------------------------------

```cmake
add_subdirectory("path/to/cppgraph")
```

После этого в системе сборки CMake будет доступна цель `CppGraph::cppgraph`, которую можно использовать при подключении библиотек:

```cmake
add_executable(program program.cpp)
target_link_libraries(program PRIVATE CppGraph::cppgraph)
```

Требования
==========

1.  Любой компилятор, который поддерживает стандарт C++17 не хуже, чем GCC 7.3 или Clang 6.0;
2.  Библиотека тестирования [doctest](https://github.com/onqtam/doctest) [Не обязательно\*];
3.  [Doxygen](http://doxygen.nl) [Не обязательно].

> \*) Можно миновать этап сборки и тестирования, если при сборке с помощью `CMake` выключить опцию `CPPGRAPH_TESTING`:
>
> ```shell
> cmake -DCMAKE_BUILD_TYPE=Release path/to/cppgraph -DCPPGRAPH_TESTING=OFF
> ```
>
> Также тестирование автоматически отключается в случае, если CppGraph подключается в качестве подпроекта.
