#pragma once

#include <boost/range/adaptor/map.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/iterator_range.hpp>

#include <initializer_list>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>

namespace cppgraph
{
    /*!
        \brief
            Ориентированный граф

        \see make_directed_graph
    */
    template <typename Vertex>
    struct directed_graph_t
    {
        using vertex_type = Vertex;
        using edge_type = typename std::unordered_multimap<Vertex, Vertex>::value_type;

        std::unordered_set<Vertex> vertices;
        std::unordered_multimap<Vertex, Vertex> edges;
    };

    /*!
        \brief
            Создать граф из списка веток
        \details
            Действительным считается только тот граф, который создан при помощи этой функции.
            Создание графа вручную — прямым вызовом конструктора — не рекомендуется.
        \returns
            Экземпляр класса `directed_graph_t<>`.
        \see directed_graph_t
    */
    template <typename Vertex>
    auto make_directed_graph (std::initializer_list<std::pair<Vertex, Vertex>> edges)
    {
        auto vertices = std::unordered_set<Vertex>{};
        for (const auto & [source, destination]: edges)
        {
            vertices.insert(source);
            vertices.insert(destination);
        }
        return directed_graph_t<Vertex>{std::move(vertices), {edges.begin(), edges.end()}};
    }

    /*!
        \brief
            Создать граф из диапазона рёбер
        \details
            Принимает диапазон пар, задающих рёбра будущего графа.
            Тип вершины элемента выводится из типа элементов пары (они должны быть одинаковы).
        \returns
            Экземпляр класса `directed_graph_t<>`.
        \see directed_graph_t
     */
    template <typename InputRange>
    auto make_directed_graph (const InputRange & edges_list)
    {
        using pair_type = typename boost::range_value<InputRange>::type;
        using first_type = typename pair_type::first_type;
        using second_type = typename pair_type::second_type;
        static_assert(std::is_same_v<std::decay_t<first_type>, std::decay_t<second_type>>);

        using vertex_type = first_type;

        auto edges =
            std::unordered_multimap<vertex_type, vertex_type>
            {
                edges_list.begin(),
                edges_list.end()
            };

        auto vertices = std::unordered_set<vertex_type>{};
        for (const auto & [source, destination]: edges)
        {
            vertices.insert(source);
            vertices.insert(destination);
        }
        return directed_graph_t<vertex_type>{std::move(vertices), std::move(edges)};
    }

    /*!
        \brief
            Добавляет вершину в граф
        \returns
            Изменённый граф, содержащий новую вершину.
        \see directed_graph_t
        \see add_edge
    */
    template <typename Vertex>
    directed_graph_t<Vertex> & add_vertex (directed_graph_t<Vertex> & g, const Vertex & v)
    {
        g.vertices.insert(v);
        return g;
    }

    /*!
        \brief
            Добавляет новое ребро в граф.
        \param from
            Начало дуги.
        \param to
            Конец дуги.
        \returns
            Изменённый граф, содержащий новую вершину.
        \see directed_graph_t
        \see add_vertex
    */
    template <typename Vertex>
    directed_graph_t<Vertex> &
        add_edge (directed_graph_t<Vertex> & g, const Vertex & from, const Vertex & to)
    {
        add_vertex(g, from);
        add_vertex(g, to);
        g.edges.insert(std::make_pair(from, to));
        return g;
    }

    /*!
        \returns
            Возвращает диапазон, содержащий все вершины графа.
        \details
            Порядок рёбер в диапазоне не определён.
        \see directed_graph_t
    */
    template <typename Vertex>
    auto vertices (const directed_graph_t<Vertex> & g)
    {
        return boost::make_iterator_range(g.vertices.begin(), g.vertices.end());
    }

    /*!
        \returns
            Возвращает диапазон, содержащий все рёбра графа.
        \details
            Порядок рёбер в диапазоне не определён.
        \see directed_graph_t
    */
    template <typename Vertex>
    auto edges (const directed_graph_t<Vertex> & g)
    {
        return boost::make_iterator_range(g.edges.begin(), g.edges.end());
    }

    /*!
        \returns
            Истину, если в графе есть искомая вершина, и ложь в противном случае.
        \see directed_graph_t
    */
    template <typename Vertex>
    bool has_vertex (const directed_graph_t<Vertex> & g, const Vertex & v)
    {
        return g.vertices.find(v) != g.vertices.end();
    }

    /*!
        \returns
            Возвращает диапазон, содержащий все вершины, в которые есть дуги из заданной вершины.
        \details
            Порядок вершин в диапазоне не определён.
        \see directed_graph_t
    */
    template <typename Vertex>
    auto direct_successors (const directed_graph_t<Vertex> & g, const Vertex & v)
    {
        return boost::make_iterator_range(g.edges.equal_range(v)) | boost::adaptors::map_values;
    }
}
