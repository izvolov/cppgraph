#pragma once

#include <unordered_map>

namespace cppgraph
{
    /*!
        \brief
            Таблица цветов для вершин графа
        \details
            Предоставляет удобный интерфейс для окраски вершин графа. Это периодически требуется
            в различных графовых алгоритмах.
        \tparam Vertex
            Тип вершин, которые нужно будет окрашивать.
        \tparam Color
            Перечисление, содержащее цвета, в которые могут быть окрашены вершины.
        \tparam DefaultColor
            Цвет по умолчанию. Будет возвращён тогда, когда вершина ещё окрашена явно в какой-либо
            цвет.
    */
    template <typename Vertex, typename Colour, Colour DefaultColour>
    struct colour_map_t
    {
        static constexpr auto default_colour = DefaultColour;

        /*!
            \brief
                Окрасить вершину в заданный цвет
        */
        template <typename V>
        void mark (const V & v, Colour colour)
        {
            marks[v] = colour;
        }

        /*!
            \brief
                Выяснить текущий цвет вершины
        */
        template <typename V>
        Colour mark (const V & v) const
        {
            if (const auto mark = marks.find(v); mark != marks.end())
            {
                return mark->second;
            }
            else
            {
                return default_colour;
            }
        }

        std::unordered_map<Vertex, Colour> marks;
    };
} // namespace cppgraph
