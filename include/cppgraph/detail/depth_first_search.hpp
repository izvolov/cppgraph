#pragma once

#include <cppgraph/colour_map.hpp>
#include <cppgraph/directed_graph.hpp>

#include <cassert>
#include <stack>
#include <utility>

namespace cppgraph::detail
{
    enum class dfs_colour_t
    {
        white,
        grey,
        black
    };

    template <typename Vertex>
    using dfs_colour_map_t = colour_map_t<Vertex, dfs_colour_t, dfs_colour_t::white>;

    template <typename Vertex, typename ColourMap, typename DFSVisitor>
    auto
        depth_first_search_impl
        (
            const directed_graph_t<Vertex> & g,
            const Vertex & root,
            ColourMap & colour,
            DFSVisitor visit
        )
    {
        using direct_successors_range = decltype(direct_successors(g, std::declval<Vertex>()));
        std::stack<std::pair<Vertex, direct_successors_range>> stack;

        // Вход в вершину.
        visit.discover_vertex(g, root);
        stack.emplace(root, direct_successors(g, root));
        colour.mark(root, dfs_colour_t::grey);

        while (not stack.empty())
        {
            auto & [head, tails] = stack.top();
            if (not tails.empty())
            {
                const auto & tail = tails.front();
                assert(colour.mark(head) == dfs_colour_t::grey);

                tails.advance_begin(1);

                switch (colour.mark(tail))
                {
                    case dfs_colour_t::white:
                    {
                        // Вход в вершину.
                        visit.discover_vertex(g, tail);
                        stack.emplace(tail, direct_successors(g, tail));
                        colour.mark(tail, dfs_colour_t::grey);

                        visit.tree_edge(g, head, tail);
                    } break;
                    case dfs_colour_t::grey:
                    {
                        visit.back_edge(g, head, tail);
                    } break;
                    case dfs_colour_t::black:
                    {
                        visit.forward_or_cross_edge(g, head, tail);
                    };
                }
            }
            else
            {
                // Выход из вершины.
                visit.finish_vertex(g, head);
                colour.mark(head, dfs_colour_t::black);
                stack.pop();
            }
        }

        return visit;
    }
} // namespace cppgraph::detail
