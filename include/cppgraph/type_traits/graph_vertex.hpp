#pragma once

#include <cppgraph/type_traits/graph_traits.hpp>

namespace cppgraph
{
    template <typename Graph>
    using graph_vertex_t = typename graph_traits<Graph>::vertex_type;
}
