#pragma once

#include <type_traits>

namespace cppgraph::detail
{
    template <typename Graph, typename = void>
    struct has_vertex_type: std::false_type {};

    template <typename Graph>
    struct has_vertex_type<Graph, std::void_t<typename Graph::vertex_type>>: std::true_type {};

    template <typename Graph>
    constexpr auto has_vertex_type_v = has_vertex_type<Graph>::value;

    template <typename Graph, typename = void>
    struct has_edge_type: std::false_type {};

    template <typename Graph>
    struct has_edge_type<Graph, std::void_t<typename Graph::edge_type>>: std::true_type {};

    template <typename Graph>
    constexpr auto has_edge_type_v = has_edge_type<Graph>::value;

    template <typename Graph, typename = void>
    struct graph_traits_impl
    {
        // Пусто во имя поддержки SFINAE.
    };

    template <typename Graph>
    struct graph_traits_impl
    <
        Graph,
        std::enable_if_t<has_vertex_type_v<Graph> && has_edge_type_v<Graph>>
    >
    {
        using vertex_type = typename Graph::vertex_type;
        using edge_type = typename Graph::edge_type;
    };
} // namespace cppgraph::detail
