#pragma once

#include <cppgraph/type_traits/detail/graph_traits.hpp>

namespace cppgraph
{
    template <typename G>
    struct graph_traits: detail::graph_traits_impl<G> {};
}
