#pragma once

#include <cppgraph/type_traits/graph_traits.hpp>

namespace cppgraph
{
    template <typename Graph>
    using graph_edge_t = typename graph_traits<Graph>::edge_type;
}
