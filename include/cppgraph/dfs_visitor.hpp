#pragma once

namespace cppgraph
{
    /*!
        \brief
            Посетитель вершин и рёбер для поиска в глубину.
        \details
            Тривиальный посетитель, обладающий заглушками для всех видов посещения вершин и рёбер.

            \note [https://ru.wikipedia.org/wiki/Поиск_в_глубину](https://ru.wikipedia.org/wiki/Поиск_в_глубину)

            Можно отнаследоваться от него и переопределить нужные функции:

            1.  Вход в вершину

                Вершина не была обследована ранее, вызывается непосредственно перед окрашиванием
                вершины в серый цвет.

                `discover_vertex(g, v)`

            2.  Выход из вершины

                Обследованы все исходящие дуги вершины, и возврата к этой вершине больше не будет.
                Вызывается непосредственно перед окрашиванием вершины в чёрный цвет.

                `finish_vertex(g, v)`

            3.  Переход по ребру дерева обхода к нерассмотренной ранее вершине

                `tree_edge(g, h, t)`

            4.  Переход по обратному ребру

                Обратное ребро означает наличие цикла в графе.

                `back_edge(g, h, t)`

            5.  Переход по прямой либо перекрёстной дуге

                `forward_or_cross_edge(g, h, t)`

            В рассмотренных выше примерах `g` — граф, в котором происходит поиск, `v` — встреченная
            вершина графа, `h` — начало встреченной дуги, `t` — конец встреченной дуги.

        \see depth_first_search
    */
    struct dfs_visitor_t
    {
        template <typename Graph, typename Vertex>
        void discover_vertex (const Graph &, const Vertex &) {}

        template <typename Graph, typename Vertex>
        void finish_vertex (const Graph &, const Vertex &) {}

        template <typename Graph, typename Vertex>
        void tree_edge (const Graph &, const Vertex &, const Vertex &) {}

        template <typename Graph, typename Vertex>
        void forward_or_cross_edge (const Graph &, const Vertex &, const Vertex &) {}

        template <typename Graph, typename Vertex>
        void back_edge (const Graph &, const Vertex &, const Vertex &) {}
    };
}
