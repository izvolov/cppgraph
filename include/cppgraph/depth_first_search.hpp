#pragma once

#include <cppgraph/detail/depth_first_search.hpp>
#include <cppgraph/directed_graph.hpp>

#include <utility>

namespace cppgraph
{
    /*!
        \brief
            Обход графа в глубину
        \param g
            Граф, который нужно обойти.
        \param visit
            Посетитель вершин и рёбер графа.
        \see dfs_visitor_t
     */
    template <typename Vertex, typename DFSVisitor>
    DFSVisitor depth_first_search (const directed_graph_t<Vertex> & g, DFSVisitor visit)
    {
        detail::dfs_colour_map_t<Vertex> colour;

        for (const auto & vertex: vertices(g))
        {
            // Либо вершина не посещена, тогда она белая, либо уже посещена со всем
            // своим подграфом, тогда она чёрная. Серых здесь быть не может.
            assert
            (
                colour.mark(vertex) == detail::dfs_colour_t::white ||
                colour.mark(vertex) == detail::dfs_colour_t::black
            );

            if (colour.mark(vertex) == detail::dfs_colour_t::white)
            {
                auto new_visit = depth_first_search_impl(g, vertex, colour, std::move(visit));
                visit = std::move(new_visit);
            }
        }

        return visit;
    }

    /*!
        \brief
            Обход в глубину подграфа, задаваемого вершиной
        \param g
            Граф, подграф которого нужно обойти.
        \param root
            Вершина, задающая корень подграфа, по которому будет непосредственно произведён обход.
        \param visit
            Посетитель вершин и рёбер графа.
        \details
            Обходит в глубину подграф графа `g`, растущий из заданной вершины `root`. Вершины
            и рёбра исходного графа, недостижимые из вершины `root`, останутся непосещёнными.
     */
    template <typename Vertex, typename DFSVisitor>
    auto
        depth_first_search
        (
            const directed_graph_t<Vertex> & g,
            const Vertex & root,
            DFSVisitor && visit
        )
    {

        detail::dfs_colour_map_t<Vertex> colour;

        return depth_first_search_impl(g, root, colour, std::forward<DFSVisitor>(visit));
    }
}
