###################################################################################################
##
##      Информация о проекте
##
###################################################################################################

cmake_minimum_required(VERSION 3.14)
project(CppGraph VERSION 0.1 LANGUAGES CXX)

get_directory_property(IS_SUBPROJECT PARENT_DIRECTORY)

###################################################################################################
##
##      Опции проекта
##
###################################################################################################

option(CPPGRAPH_TESTING "Включить модульное тестирование" ON)
option(CPPGRAPH_COVERAGE "Включить измерение покрытия кода тестами" OFF)

###################################################################################################
##
##      Опции компиляции
##
###################################################################################################

add_compile_options(
    -Werror

    -Wall
    -Wextra
    -Wpedantic

    -Wcast-align
    -Wcast-qual
    -Wconversion
    -Wctor-dtor-privacy
    -Wenum-compare
    -Wfloat-equal
    -Wnon-virtual-dtor
    -Wold-style-cast
    -Woverloaded-virtual
    -Wredundant-decls
    -Wsign-conversion
    -Wsign-promo
)

if(NOT CMAKE_CXX_EXTENSIONS)
    set(CMAKE_CXX_EXTENSIONS OFF)
endif()

if(NOT CMAKE_BUILD_TYPE)
    message(WARNING "Тип сборки не выбран (см. опцию CMAKE_BUILD_TYPE).")
else()
    message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")
endif()

###################################################################################################
##
##      Заголовочная библиотека
##
###################################################################################################

add_library(cppgraph_headers INTERFACE)
target_include_directories(cppgraph_headers INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)
target_compile_features(cppgraph_headers INTERFACE cxx_std_17)
set_target_properties(cppgraph_headers PROPERTIES EXPORT_NAME cppgraph)

add_library(CppGraph::cppgraph ALIAS cppgraph_headers)

###################################################################################################
##
##      Буст
##
###################################################################################################

find_package(Boost 1.65.1 REQUIRED)
target_link_libraries(cppgraph_headers INTERFACE Boost::boost)

###################################################################################################
##
##      Установка
##
###################################################################################################

install(DIRECTORY include/cppgraph DESTINATION include)

install(TARGETS cppgraph_headers EXPORT CppGraphConfig)
install(EXPORT CppGraphConfig NAMESPACE CppGraph:: DESTINATION share/CppGraph/cmake)

include(CMakePackageConfigHelpers)
write_basic_package_version_file("${PROJECT_BINARY_DIR}/CppGraphConfigVersion.cmake"
    VERSION
        ${PROJECT_VERSION}
    COMPATIBILITY
        AnyNewerVersion
)
install(FILES "${PROJECT_BINARY_DIR}/CppGraphConfigVersion.cmake" DESTINATION share/CppGraph/cmake)

###################################################################################################
##
##      Тесты
##
###################################################################################################

if(NOT CPPGRAPH_TESTING)
    message(STATUS "Тестирование проекта CppGraph выключено")
elseif(IS_SUBPROJECT)
    message(STATUS "CppGraph не тестируется в режиме подмодуля")
else()
    add_subdirectory(test)
endif()

###################################################################################################
##
##      Документация
##
###################################################################################################

if(NOT IS_SUBPROJECT)
    add_subdirectory(doc)
endif()

###################################################################################################
##
##      Онлайн-песочница
##
###################################################################################################

if(NOT IS_SUBPROJECT)
    add_subdirectory(online)
endif()
