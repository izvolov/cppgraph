#include <cppgraph/directed_graph.hpp>
#include <cppgraph/type_traits/graph_vertex.hpp>

#include <doctest/doctest.h>

#include <algorithm>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

TEST_CASE("Ориентированный граф создаётся при помощи функции \"make_directed_graph\"")
{
    // V = {A, B, C, D}
    // E = {A -> B, A -> C, B -> C, B -> D}
    const auto graph =
        cppgraph::make_directed_graph<std::string>
        ({
            {"A", "B"}, {"A", "C"},
            {"B", "C"},
            {"B", "D"}
        });

    CHECK(not vertices(graph).empty());
    CHECK(not edges(graph).empty());
}

TEST_CASE("Содержит без повторений все вершины, содержащиеся в рёбрах, "
    "переданных в функцию \"make_directed_graph\"")
{
    // V = {A, B, C, D}
    // E = {A -> B, A -> C, B -> C, B -> D}
    const auto initial_edges =
        std::vector<std::pair<std::string, std::string>>
        {
            {"A", "B"}, {"A", "C"},
            {"B", "C"},
            {"B", "D"}
        };

    std::vector<std::string> initial_vertices;
    const auto heads = initial_edges | boost::adaptors::map_keys;
    const auto tails = initial_edges | boost::adaptors::map_values;
    initial_vertices.insert(initial_vertices.end(), heads.begin(), heads.end());
    initial_vertices.insert(initial_vertices.end(), tails.begin(), tails.end());
    std::sort(initial_vertices.begin(), initial_vertices.end());
    initial_vertices.erase
    (
        std::unique(initial_vertices.begin(), initial_vertices.end()),
        initial_vertices.end()
    );

    const auto graph = cppgraph::make_directed_graph(initial_edges);

    auto actual = std::vector<std::string>(vertices(graph).begin(), vertices(graph).end());

    std::sort(actual.begin(), actual.end());
    CHECK(actual == initial_vertices);
}

TEST_CASE("Содержит все рёбра, переданные в функцию \"make_directed_graph\"")
{
    // V = {A, B, C, D}
    // E = {A -> B, A -> C, B -> C, B -> D}
    auto initial_edges =
        std::vector<std::pair<std::string, std::string>>
        {
            {"A", "B"}, {"A", "C"},
            {"B", "C"},
            {"B", "D"}
        };

    const auto graph = cppgraph::make_directed_graph(initial_edges);

    auto actual =
        std::vector<std::pair<std::string, std::string>>(edges(graph).begin(), edges(graph).end());
    std::sort(actual.begin(), actual.end());

    std::sort(initial_edges.begin(), initial_edges.end());
    CHECK(actual == initial_edges);
}

TEST_CASE("Содержит вершины явно, т.е. в том виде, в котором они были указаны при создании графа")
{
    // V = {1, 2}
    // E = {1 -> 2}
    const auto graph =
        cppgraph::make_directed_graph<int>
        ({
            {1, 2}
        });

    CHECK(cppgraph::has_vertex(graph, 2));
    CHECK(cppgraph::direct_successors(graph, 2).empty());
}

TEST_CASE("При добавлении ребра, в граф также добавляются и обе вершины этого ребра")
{
    using namespace std::literals::string_literals;

    auto graph = cppgraph::make_directed_graph<std::string>({});

    cppgraph::add_edge(graph, "1"s, "2"s);

    CHECK(cppgraph::has_vertex(graph, "1"s));
    CHECK(cppgraph::has_vertex(graph, "2"s));
}

TEST_CASE("Умеет создаваться из диапазона пар")
{
    const auto edges =
        std::vector<std::pair<int, int>>
        {
            {4, 5}, {4, 9},
            {5, 1},
            {7, 3}
        };

    auto graph = cppgraph::make_directed_graph(edges);

    using vertex_type = cppgraph::graph_vertex_t<decltype(graph)>;
    CHECK((std::is_same<vertex_type, int>::value));
}
