#include <cppgraph/depth_first_search.hpp>
#include <cppgraph/dfs_visitor.hpp>
#include <cppgraph/directed_graph.hpp>

#include <doctest/doctest.h>

#include <set>
#include <utility>

namespace
{
    struct cycle_detector: cppgraph::dfs_visitor_t
    {
        template <typename Graph, typename Vertex>
        void back_edge (const Graph &, const Vertex &, const Vertex &)
        {
            cycle_is_detected = true;
        }

        bool cycle_is_detected = false;
    };

    template <typename Vertex>
    struct vertex_discoverer: cppgraph::dfs_visitor_t
    {
        template <typename Graph>
        void discover_vertex (const Graph &, const Vertex & v)
        {
            vertices.insert(v);
        }

        std::multiset<Vertex> vertices;
    };

    template <typename Vertex>
    struct vertex_finisher: cppgraph::dfs_visitor_t
    {
        template <typename Graph>
        void finish_vertex (const Graph &, const Vertex & v)
        {
            vertices.insert(v);
        }

        std::multiset<Vertex> vertices;
    };

    template <typename Vertex>
    struct edge_visitor: cppgraph::dfs_visitor_t
    {
        template <typename Graph>
        void tree_edge (const Graph &, const Vertex & head, const Vertex & tail)
        {
            tree_edges.emplace(head, tail);
            all_edges.emplace(head, tail);
        }

        template <typename Graph>
        void forward_or_cross_edge (const Graph &, const Vertex & head, const Vertex & tail)
        {
            forward_or_cross_edges.emplace(head, tail);
            all_edges.emplace(head, tail);
        }

        template <typename Graph>
        void back_edge (const Graph &, const Vertex & head, const Vertex & tail)
        {
            back_edges.emplace(head, tail);
            all_edges.emplace(head, tail);
        }

        std::multiset<std::pair<Vertex, Vertex>> tree_edges;
        std::multiset<std::pair<Vertex, Vertex>> forward_or_cross_edges;
        std::multiset<std::pair<Vertex, Vertex>> back_edges;
        std::multiset<std::pair<Vertex, Vertex>> all_edges;
    };
}

TEST_CASE("Заходит в каждую вершину графа ровно один раз")
{
    const auto graph =
        cppgraph::make_directed_graph<int>
        ({
            {0, 1}, {0, 2}, {0, 4},
            {1, 3},
            {2, 3}, {2, 4}
        });


    const auto discovered = cppgraph::depth_first_search(graph, vertex_discoverer<int>{});

    const auto expected = std::multiset<int>{vertices(graph).begin(), vertices(graph).end()};
    CHECK(discovered.vertices == expected);
}

TEST_CASE("Выходит из каждой вершины графа ровно один раз")
{
    const auto graph =
        cppgraph::make_directed_graph<int>
        ({
            {0, 1}, {0, 2}, {0, 4},
            {1, 2}, {1, 3},
            {2, 0}, {2, 3}, {2, 4}
        });

    const auto finished = cppgraph::depth_first_search(graph, vertex_finisher<int>{});

    const auto expected = std::multiset<int>{vertices(graph).begin(), vertices(graph).end()};
    CHECK(finished.vertices == expected);
}

TEST_CASE("Обходит все рёбра графа ровно один раз")
{
    const auto graph =
        cppgraph::make_directed_graph<int>
        ({
            {0, 1}, {0, 2}, {0, 4},
            {1, 2}, {1, 3},
            {2, 0}, {2, 3}, {2, 4}
        });

    const auto result = cppgraph::depth_first_search(graph, edge_visitor<int>{});

    const auto expected =
        std::multiset<std::pair<int, int>>{edges(graph).begin(), edges(graph).end()};
    CHECK(result.all_edges == expected);
    CHECK(not result.tree_edges.empty());
    CHECK(not result.forward_or_cross_edges.empty());
    CHECK(not result.back_edges.empty());
}

TEST_CASE("Успешно завершается при наличии циклов в графе")
{
    const auto g =
        cppgraph::make_directed_graph<int>
        ({
            {0, 1},
            {1, 2},
            {2, 0}
        });

    const auto result = cppgraph::depth_first_search(g, cycle_detector{});

    CHECK(result.cycle_is_detected);
}
