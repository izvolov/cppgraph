#include <cppgraph/depth_first_search.hpp>
#include <cppgraph/dfs_visitor.hpp>
#include <cppgraph/directed_graph.hpp>

#include <cassert>
#include <set>

template <typename Vertex>
struct vertex_discoverer: cppgraph::dfs_visitor_t
{
    template <typename Graph>
    void discover_vertex (const Graph &, const Vertex & v)
    {
        vertices.insert(v);
    }

    std::multiset<Vertex> vertices;
};

int main ()
{
    const auto graph =
        cppgraph::make_directed_graph<int>
        ({
            {0, 1}, {0, 2}, {0, 4},
            {1, 3},
            {2, 3}, {2, 4}
        });

    const auto discovered = cppgraph::depth_first_search(graph, vertex_discoverer<int>{});

    const auto expected = std::multiset<int>{vertices(graph).begin(), vertices(graph).end()};
    assert(discovered.vertices == expected);
}
